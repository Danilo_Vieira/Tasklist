package poc.TaskList;

import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import poc.TaskList.model.Task;
import poc.TaskList.model.TaskStatus;
import poc.TaskList.repository.StatusRepository;
import poc.TaskList.repository.TaskRepository;

@SpringBootApplication
public class TaskListApplication {

	@Bean
	public WebMvcConfigurerAdapter corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/tasks/**")
						.allowedOrigins("*")
						.allowedMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
			}
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(TaskListApplication.class, args);		
	}
	
	@Bean
	public CommandLineRunner init(TaskRepository repo, StatusRepository statusRepo) {
		return (arg) -> {
			
			TaskStatus status = new TaskStatus();
			status.setAtivo(true);
			status.setDescricao("Ativa");
			
			statusRepo.save(status);
			
			TaskStatus status2 = new TaskStatus();
			status2.setAtivo(true);
			status2.setDescricao("Em Espera");
			
			statusRepo.save(status2);
			
			TaskStatus status3 = new TaskStatus();
			status3.setAtivo(true);
			status3.setDescricao("Concluida");
			
			statusRepo.save(status3);
			
			
			Date dataCricacao = new Date();
			
			Task task1 = new Task();
			task1.setTitulo("TASK 1");
			task1.setDescricao("nova task1");
			task1.setObservacao("criado na inicializacao do servico");
			task1.setStatus(status);
			task1.setCriacao(dataCricacao);
			
			repo.save(task1);
			
			Task task2 = new Task();
			task2.setTitulo("TASK 2");
			task2.setDescricao("nova task2");
			task2.setObservacao("criado na inicializacao do servico");
			task2.setStatus(status2);
			task2.setCriacao(dataCricacao);
			
			repo.save(task2);
			
			Task task3 = new Task();
			task3.setTitulo("TASK 3");
			task3.setDescricao("nova task3");
			task3.setObservacao("criado na inicializacao do servico");
			task3.setStatus(status3);
			task3.setCriacao(dataCricacao);
			
			repo.save(task3);			
			
		};
	}

}
