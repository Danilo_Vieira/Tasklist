package poc.TaskList.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import poc.TaskList.model.Task;
import poc.TaskList.model.TaskStatus;
import poc.TaskList.service.TaskService;

/**
 * 
 * @author Danilo
 *
 */
@RestController
//@CrossOrigin("${permited-origin}") // opção de CORS com arquivo de configuracao
@RequestMapping(value = "/")
public class TaskRest {
	
	@Autowired
	private TaskService service;

	@PostMapping("/tasks")
	public @ResponseBody Task save(@RequestBody Task task) {
		try {
			service.save(task);
			System.out.println("salvar");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return task;
	}
	
	@PutMapping("/tasks/{id}")
	public @ResponseBody Task update(@RequestBody Task task) {
		try {
			service.update(task);
			System.out.println("update");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return task;
	}
	
	@GetMapping("/tasks/{id}")
	public @ResponseBody Task editar(@PathVariable("id") Integer id) {
		Task task = null;
		//Task clone = new Task();
		try {
			task = service.find(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return task;
	}
	
	@DeleteMapping("/tasks/{id}")
	public @ResponseBody Task deletar(@PathVariable("id") Integer id) {
		Task task = null;
		try {
			service.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return task;
	}
	
	@GetMapping("/tasks/listaStatus")
	public @ResponseBody List<TaskStatus> listarStatus (){
		
		List<TaskStatus> status = new ArrayList<TaskStatus>();
		try {
			status.addAll(service.listStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	@GetMapping("/tasks")
	public @ResponseBody List<Task> test() {
		List<Task> tasks = new ArrayList<Task>();
		try {
			tasks.addAll(service.list());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tasks;
	}
	
//	@GetMapping("/tasks/hello/{param1}/{param2}")
//	public @ResponseBody  String hello(@PathVariable("param1") String param1, @PathVariable("param2") String param2) {
//		System.out.println(param1+" "+param2);
//		return "hello!!!";
//	}
//	
//	@GetMapping("/tasks/hello2")
//	public @ResponseBody  String hello2( WebRequest webRequest) {
//		String param2 = webRequest.getParameter("param2");
//		
//		System.out.println(param2);
//		return "hello!!!";
//	}

}
