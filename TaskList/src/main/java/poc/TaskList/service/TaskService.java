package poc.TaskList.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import poc.TaskList.model.Task;
import poc.TaskList.model.TaskStatus;
import poc.TaskList.repository.StatusRepository;
import poc.TaskList.repository.TaskRepository;

@Service
public class TaskService {
	
	@Autowired	
	private TaskRepository taskRepository;
	
	@Autowired
	private StatusRepository statusRepository;
	
	public Task save(Task task) throws Exception{
		
		try {
			task.setCriacao(new Date());
			taskRepository.save(task);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return task;
	}
	
	public Task update(Task task) throws Exception {
		try {
			if(task.getId() != 0) {
				task.setEdicao(new Date());
			}
			taskRepository.save(task);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		return task;
	}

	public List<Task> list() throws Exception{
		try {
			return taskRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	public Task find(Integer id) throws Exception {
		try {
			return taskRepository.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	public List<TaskStatus> listStatus()  throws Exception {
		try {
			return statusRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	public void delete(Integer id) throws Exception {
		try {
			taskRepository.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	

	

}
