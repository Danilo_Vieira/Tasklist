package poc.TaskList.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
 /**
  * 
  * @author Danilo
  *
  */
@Entity
@Table(name = "tb_task")
public class Task implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -1992441638785505886L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String titulo;
	private String descricao;
	private String observacao;
	
	@ManyToOne
    @JoinColumn(name = "id_task_type")
	private TaskStatus status;
	
	//TODO criar uma tabela com data e tipo EX. dataCriacao, Exclusao, inicio; para ter o historico de alteracoes da task. 
	private Date criacao;
	private Date edicao;
	private Date remocao;
	private Date conclusao;
	private Date inicio;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy") //TODO 
	private Date previsaoConclusao;	

	public Task(String titulo, Date criacao) {
		super();
		this.titulo = titulo;
		this.criacao = criacao;
	}
	
	public Task() {
		
	}
		
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public TaskStatus getStatus() {
		return status;
	}
	public void setStatus(TaskStatus status) {
		this.status = status;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getCriacao() {
		return criacao;
	}
	public void setCriacao(Date criacao) {
		this.criacao = criacao;
	}
	public Date getEdicao() {
		return edicao;
	}
	public void setEdicao(Date edicao) {
		this.edicao = edicao;
	}
	public Date getRemocao() {
		return remocao;
	}
	public void setRemocao(Date remocao) {
		this.remocao = remocao;
	}
	public Date getConclusao() {
		return conclusao;
	}
	public void setConclusao(Date conclusao) {
		this.conclusao = conclusao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Date getPrevisaoConclusao() {
		return previsaoConclusao;
	}
	public void setPrevisaoConclusao(Date previsaoConclusao) {
		this.previsaoConclusao = previsaoConclusao;
	}
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

}
