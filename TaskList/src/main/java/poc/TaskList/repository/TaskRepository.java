package poc.TaskList.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import poc.TaskList.model.Task;

public interface TaskRepository extends JpaRepository<Task, Integer>, TaskRepositoryQuery {

}
