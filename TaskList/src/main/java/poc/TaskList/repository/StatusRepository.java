package poc.TaskList.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import poc.TaskList.model.TaskStatus;

public interface StatusRepository extends JpaRepository<TaskStatus, Integer>, TaskRepositoryQuery {

}
