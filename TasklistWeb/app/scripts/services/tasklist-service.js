angular.module('appServices', ['ngResource'])
	.factory('appResource', function($resource){
		
		// TODO criar query string para pesquisar a lista de tipos na base de dados ao carregar a pagina (estudar como fazer)
		const url = 'http://localhost:8080/tasks/:id';
		
		return $resource(url , null, {
			'update' : {
				method: 'PUT'
			}
		});
	})
	.factory('cadastroResource', function(appResource, $q) {
		var service = {};
		service.cadastrar = function(task){
			return $q(function(resolve, reject) {
				if(task.id){
					appResource.update({id: task.id}, task, function(){
						resolve({
							mensagem: 'Task' + task.titulo + ' atualizada.',
							inclusao: false
						})
					}, function(erro) {
                        console.log(erro);
                        reject({
                            mensagem: 'Não foi possível atualizar a Task ' + task.titulo
                        });
                    });
				} else {
					appResource.save(task, function() {
                       // $rootScope.$broadcast(evento); 

                        resolve({
                            mensagem: 'Task' + task.titulo + ' incluída com sucesso',
                            inclusao: true
                        });
                    }, function(erro) {
                        console.log(erro);
                        reject({
                            mensagem: 'Não foi possível incluir a foto ' + task.titulo
                        });
                    });
                }
				
			});
		};
		
		return service;
	}).factory('statusResource', function($resource){
		
		return $resource('http://localhost:8080/tasks/listaStatus');
		
	})
//	.factory('testResource', function($resource){
//		
//		var param = {param1: '@param1', param2: '@param2' };
//		
//		return $resource('http://localhost:8080/tasks/hello/:param1/:param2', param);
//		
//	})
//	.factory('testResource2', function($resource){
//		
//		// {someParam: '@someProp'} then the value of someParam will be data.someProp
//		
//		var param = {param1: '@param1', param2: '@param2' };
//		
//		return $resource('http://localhost:8080/tasks/hello2/', param);
//		
//	})
	;
	
	
	

