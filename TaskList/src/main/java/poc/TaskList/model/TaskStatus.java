package poc.TaskList.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Danilo
 *
 */
@Entity 
@Table(name = "tb_task_tatus")
public class TaskStatus implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7189873915408933406L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String descricao;
	private boolean ativo;
	
//	@OneToMany(mappedBy = "taskType", cascade = CascadeType.ALL)
//	private List<Task> Tasks;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	
}
