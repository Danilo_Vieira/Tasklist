angular.module('tasklistWeb')
  .controller('tasklist-controller', function ($scope,$http, $routeParams, appResource, cadastroResource, statusResource) {

    const TAB_CADASTRO = 1;
    const TAB_PESQUISA = 2;
    const TIPO_MSG_ALERTA = 'danger';
    const TIPO_MSG_SUCESSO = 'success';
    const TIPO_MSG_ANTECAO = 'warning';
    const TIPO_MSG_INFO = 'info';

    var abaAtual = TAB_PESQUISA;
    var ng = $scope;

  	ng.lista_status = [];
  	ng.taskList = [];

    ng.itemExclusao = {};
    var indiceEdicao = null;

    $('#div-dt-inicio').datepicker({
    	format: "dd/mm/yyyy",
    	language: "pt-BR",
    	clearBtn: true,
    	todayHighlight: true,
    	//container: '#bt-dt-inicio',
    	forceParse: false

    }).on('changeDate', function(e) {
    	console.log($('#dt-inicio').val());
    });

    $('#dt-inicio').mask("99/99/9999");

    // Ex. regex para validar data inclusive ano bisexto
    /*onClickTest = function(){
    	var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;

    	console.log( dateRegex.test( $('#dt-inicio').val() ) );

    }*/

    // Metodo que trata o evento de click do botao salvar
    ng.onClickBtnSalvar = function(){

        if(validarCampos() && verificarCadastroRepedito()){
        	//TODO verificar uma maneira melhor de fazer
        	ng.task.previsaoConclusao = $('#dt-inicio').val();
        	cadastroResource.cadastrar(ng.task)
        	.then(function(dados) {
        		ng.listar();
				ng.mensagem = dados.mensagem;
				if(dados.inclusao) ng.task = {};
			})
        	.catch(function(erro) {
				ng.mensagem = erro.mensagem;
			})

            ng.onClickBtnLimparCampos();
            onControlarTab(TAB_PESQUISA);
            onExibirNotificacao("Task salva com sucesso.", "ATENÇÃO", TIPO_MSG_SUCESSO);
        }

     }

    // lista todas as tasks
    ng.listar = function(){
    	appResource.query(function(taskList) {
    	        ng.taskList = taskList;
    	    }, function(erro) {
    	        console.log(erro);
    	    })
    };

    // lista todas os status
    ng.listarStatus = function(){
    	statusResource.query({recurso: 'listaStatus'}, function(lista_status) {
    	        ng.lista_status = lista_status;
    	    }, function(erro) {
    	        console.log(erro);
    	    })
    };

//    ng.test = function(){
//    	testResource.get( {param1: 'hello', param2: 'server'} , function(resposta){
//    		alert(resposta);
//    	}, function(erro){
//    		console(erro);
//    	})
//    };
//
//
//    // Ex: query string
//    ng.test2 = function(){
//    	testResource2.get( {param1: 'hello', param2: 'server'} , function(resposta){
//    		alert(resposta);
//    	}, function(erro){
//    		console(erro);
//    	})
//    };



    // METODO PARA LIMPAR OS CAMPOS DE FORMULARIO
    ng.onClickBtnLimparCampos = function(){
        ng.task = {};
        $('#dt-inicio').datepicker('update', '');
        indiceEdicao = null;
        removerBordaVermelhaDeCamposNaoPreenchidos();
    }


     // METODO PARA EXIBIR TOASTER DE NOTIFICACAO
     // msg: mensagem da notificação
     // titulo: titulo da notificação
     // tipo: tipo da notificação (TIPO_MSG_ALERTA, TIPO_MSG_INFO, TIPO_MSG_SUCESSO, TIPO_MSG_ATENCAO)
     onExibirNotificacao = function(msg, titulo,  tipo) {
        //$.toaster({ message : msg, title : titulo, priority : tipo });
        alert(tipo);
        $.toast({
                heading: titulo,
                text : msg+tipo,
                icon: tipo

              });
    };

    // Metodo que trata o evento de click do botao editar do grid de pesquisa
    ng.onClickBtnEditar = function (id ){
        // se não usar angular.copy a alteracao ocorre direto no objeto da lista
        //ng.task = angular.copy(param);
        //indiceEdicao = index;

        appResource.get( {id: id} , function(resposta){
        	ng.task = resposta;
        	// TODO verificar uma forma melhor de fazer
        	if(resposta.previsaoConclusao){
        		var data = resposta.previsaoConclusao.split('-');
        		if(data.length == 3){
        			//ng.task.previsaoConclusao = data[2]+'/'+data[1]+'/'+data[0];
        			$('#dt-inicio').val(data[2]+'/'+data[1]+'/'+data[0]);
        		}
        	}

    	}, function(erro){
    		console(erro);
    	});

        // campos obrigatorios
        //$('.campo_obrigatorio').removeClass('campoInvalido');
        removerBordaVermelhaDeCamposNaoPreenchidos();
        onControlarTab(TAB_CADASTRO);
    }

    // METODO PARA TRATAR CLICK DE ABERTURA DE MODAL DE EXCLUSAO
    ng.onClickBtnExcluir = function(index, task){

    	ng.itemExclusao.indice = index;
    	ng.itemExclusao.id = task.id;

        $("#sp-item-exclusao-descricao").text(task.descricao);
        $("#sp-item-exclusao-titulo").text(task.titulo);
        ng.onClickBtnLimparCampos();
    }

    // METODO PARA TRATAR CLICK DE CONFIRMACAO DE EXCLUSAO NO MODAL DE EXCLUSAO
    ng.onClickConfirmarExcluir = function(index, id){


    	appResource.remove( {id: id} , function(resposta){
        	// TODO fazer tratamento de erro (try/catch)
        	ng.taskList.splice(index, 1);
        	onExibirNotificacao("Task excluído com sucesso.", "ANTEÇÃO", TIPO_MSG_SUCESSO);
    	}, function(erro){
    		console(erro);
    	});

    }

    // METODO PARA VALIDAR O PREENCHIMENTO DOS CAMPOS DE CADASTRO
    validarCampos = function(){
        var isValido = true;
        var camposObrigatorios = $(".campo-obrigatorio");

        removerBordaVermelhaDeCamposNaoPreenchidos();

       // utilizando o seletor de classe a comparação é feita somente como o primeiro elemento que tem a classe .campo-obrigatorio
       // este seletor traz um array de obj DOM que tenham a classe .campo-obrigatorio, até o monento a melhor alternativa é percorrer
       // o array para verificar o .val() de cada um.
       for (var i = camposObrigatorios.length - 1; i >= 0; i--) {
           if(camposObrigatorios[i].value.length == 0){
                isValido = false;
                $('#'+camposObrigatorios[i].id).addClass('campo-invalido');
            }
       }

       if(!isValido){
            onExibirNotificacao("Preencha os campos destacados.", "ANTEÇÃO", TIPO_MSG_ALERTA);
       }

       return isValido;
    }

    // METODO QUE REMOVE A CLASSE campoInvalido QUANDO O CAMPO É PREENCHIDO
    removerBordaVermelhaDeCamposNaoPreenchidos = function(){
        $('.campo-obrigatorio').removeClass('campo-invalido');
    }

    // METODO UTILIZADO PARA VERIFICAR REPETICAO NA LISTA DE TAREFAS
    verificarCadastroRepedito = function(){
//        var lista = angular.copy(ng.taskList);
//        var isNaoRepetido = true;
//
//        // remove o elemento da lista quando o mesmo está sendo editado para não ser compardado a ele mesmo.
//        if(indiceEdicao != null){
//            lista.splice(indiceEdicao, 1);
//        }
//
//        // USADO for NATIVO PORQUE forEach DO ANGULAR NÃO PARA COM return NEM COM break
//        for (var i = 0, len = lista.length; i < len; i++) {
//
//            if(lista[i].codigo == ng.task.codigo){
//               onExibirNotificacao("Código "+lista[i].codigo+ " já cadastrado.", "ANTEÇÃO", TIPO_MSG_ALERTA);
//               isNaoRepetido = false;
//               break;
//            }
//            if(lista[i].descricao == ng.task.descricao){
//               onExibirNotificacao("Descrição "+lista[i].descricao+ " já cadastrado.", "ANTEÇÃO", TIPO_MSG_ALERTA);
//               isNaoRepetido = false;
//               break;
//            }
//        }
//
//        return isNaoRepetido;
    	// TODO implementar pesquisa no backend para evitar cadastro repetidos
    	return true;
    }

    // METODO PARA CONTROLAR AS ABAS DE CADASTRO E DE PESQUISA
    onControlarTab = function(tab) {
        if(tab == abaAtual) {
            return;
        }

        abaAtual = tab;

        if(tab == TAB_CADASTRO) {
            $('#li-pesquisa').removeClass('active');
            $('#li-cadastro').addClass('active');

            $('#div-pesquisa').fadeOut(500, function(){
                $('#div-cadastro').fadeIn();
            });

        }
        else if(tab == TAB_PESQUISA){
            $('#li-cadastro').removeClass('active');
            $('#li-pesquisa').addClass('active');

            $('#div-cadastro').fadeOut(500, function(){
                $('#div-pesquisa').fadeIn();
            });
        }
    };

    ng.listar();

    ng.listarStatus();

  });
